# README

This test data was generated and distributed by the original fraposa developer (Daiwei Zhang).

Downloaded from https://upenn.app.box.com/v/fraposa-demo (2023-03-23).

This archive is intended to be used with the fraposa_pgsc fork's CI/CD pipeline.

Links:

https://github.com/PGScatalog/fraposa_pgsc
https://github.com/daviddaiweizhang/fraposa